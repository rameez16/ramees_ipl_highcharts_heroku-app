module.exports = {
  matchesPlayedYearly,
  teamWinPerYear,
  extrRuns,
  topTenEconomicalBowlers,
  tossWinandMatchwin,
  highestMoMperSeason,
  strikeRateperSeason,
  highestDismissalPair,
  bestEconomyinSuperOver,
 /*  highestDismissalPair,
  filterDatabyseason,
  kholiRunsbyseason,
  runsbyKholiallseason,
  top10rungettersin2013,
  top10wicketTakersin2011,
  boundariesHitbyKholiin2017,
  top10wicketTakersinalltheSeasons,
  bumbraWickettakeninALLseason */
};

//Number of matches played per year for all the years in IPL.

function matchesPlayedYearly(inputData) {
  let obj = {};

  inputData.map(function (element) {
    return obj[element.season]
      ? obj[element.season]++
      : (obj[element.season] = 1);
  });

  return obj;
}

// Number of matches won per team per year in IPL.

function teamWinPerYear(input) {
  let obj = {};

  input.map(function (element) {
    return !obj[element.winner]
      ? (obj[element.winner] = {})
      : obj[element.winner];
  });

  input.map(function (element) {
    return obj[element.winner][element.season]
      ? obj[element.winner][element.season]++
      : (obj[element.winner][element.season] = 1);
  });

  return obj;
}

//Extra runs conceded per team in the year 2016

function extrRuns(matchData, DeliveryData) {
  let idRange = matchData
    .filter((matchData) => matchData.season === "2016")
    .map((matchData) => matchData.id);
  let objData2016 = DeliveryData.filter(
    (DeliveryData) => idRange.indexOf(DeliveryData.match_id) > -1
  );

  let obj = {};

  objData2016.map((e) =>
    obj[e.bowling_team]
      ? (obj[e.bowling_team] += +e.extra_runs)
      : (obj[e.bowling_team] = +e.extra_runs)
  );

  return obj;
}

//Top 10 economical bowlers in the year 2015

function topTenEconomicalBowlers(matchData, DeliveryData) {
  let obj = {};
  let idRange = matchData
    .filter((matchData) => matchData.season === "2015")
    .map((matchData) => matchData.id);
  let input = DeliveryData.filter(
    (DeliveryData) => idRange.indexOf(DeliveryData.match_id) > -1
  );

  for (let i = 0; i < input.length; i++) {
    let ballCount = 1;

    //calculate total delivery the bowler bowled & runs he gave away , keep data in => obj

    let bowler = input[i].bowler;
    let batsmanRuns = input[i].batsman_runs;
    let noball = input[i].noball_runs;
    let wide = input[i].wide_runs;
    let runsGiven = "runsGiven";
    let noOfdeliveries = "deliveryBowled";

    let runsCalc = parseInt(batsmanRuns) + parseInt(noball) + parseInt(wide);

    if (obj[bowler]) {
      if (obj[bowler][runsGiven]) {
        obj[bowler][runsGiven] += runsCalc;
      } else {
        obj[bowler][runsGiven] = runsCalc;
      }

      if (obj[bowler][noOfdeliveries]) {
        if (noball == 0 && wide == 0) {
          obj[bowler][noOfdeliveries]++;
        }
      } else {
        if (noball == 0 && wide == 0) {
          obj[bowler][noOfdeliveries] = ballCount;
        }
      }
    } else {
      obj[bowler] = {};
    }
  }

  let objEconomy = {};

  for (let key in obj) {
    //find economy of each bowler & stor in objEconomy

    let overs = obj[key].deliveryBowled / 6;
    let economy = obj[key].runsGiven / overs;

    objEconomy[key] = economy;
  }

  let newArry = Object.entries(objEconomy);

  let sortedArr = newArry.sort((a, b) => a[1] - b[1]);

  return sortedArr.slice(0, 10);
}

//Find the number of times each team won the toss and also won the match

function tossWinandMatchwin(matchData) {
  let obj = {};
  matchData
    .filter((element) => element.toss_winner === element.winner)
    .map((element) =>
      obj[element.winner] ? obj[element.winner]++ : (obj[element.winner] = 1)
    );

  return obj;
}

// Find a player who has won the highest number of Player of the Match awards for each season

function highestMoMperSeason(input) {
  let obj = {};

  input.map((e) => (obj[e.season] ? obj[e.season] : (obj[e.season] = {})));

  input.map((e) =>
    obj[e.season][e.player_of_match]
      ? obj[e.season][e.player_of_match]++
      : (obj[e.season][e.player_of_match] = 1)
  );

  let finalObj = {};
  for (season in obj) {
    let max = 0,
      batsmanName = "";
    for (batsman in obj[season]) {
      if (obj[season][batsman] > max) {
        max = obj[season][batsman];
        playerName = batsman;
      }
    }
    finalObj[season] = playerName;
  }
  return finalObj;
}

// Find the strike rate of a batsman for each season

function strikeRateperSeason(matchData, DeliveryData) {
  let obj1 = {};
  matchData.map((e) => {
    if (obj1.hasOwnProperty(e.season)) {
      obj1[e.season]++;
    } else {
      obj1[e.season] = 1;
    }
  });

  let arr = Object.keys(obj1);

  let obj = {};

  for (var i = 0; i < arr.length; i++) {
    let idRange = matchData
      .filter((matchData) => matchData.season === arr[i])
      .map((matchData) => matchData.id);
    let input1 = DeliveryData.filter(
      (DeliveryData) => idRange.indexOf(DeliveryData.match_id) > -1
    );
    input1.map((e) =>
      obj[e.batsman] ? obj[e.batsman] : (obj[e.batsman] = {})
    );
    input1.map((e) =>
      obj[e.batsman][arr[i]]
        ? obj[e.batsman][arr[i]]
        : (obj[e.batsman][arr[i]] = {})
    );

    let runs = "Runs taken";
    let delivery = "Delivery faced";
    let strickrate = "Strikerate";

    input1.map((e) =>
      obj[e.batsman][arr[i]][runs]
        ? (obj[e.batsman][arr[i]][runs] += parseInt(e.batsman_runs))
        : (obj[e.batsman][arr[i]][runs] = parseInt(e.batsman_runs))
    );
    input1.map((e) =>
      obj[e.batsman][arr[i]][delivery]
        ? obj[e.batsman][arr[i]][delivery]++
        : (obj[e.batsman][arr[i]][delivery] = 1)
    );

    input1.map((e) =>
      obj[e.batsman][arr[i]][strickrate]
        ? obj[e.batsman][arr[i]][strickrate]
        : (obj[e.batsman][arr[i]][strickrate] =
            (obj[e.batsman][arr[i]][runs] / obj[e.batsman][arr[i]][delivery]) *
            100)
    );
  }
  return obj;
}

//Find the highest number of times one player has been dismissed by another player

function highestDismissalPair(DeliveryData) {
  let obj = {};

  DeliveryData.map((e) => {
    if (e.player_dismissed.length != 0 && e.dismissal_kind != "run out") {
      if (obj.hasOwnProperty(e.batsman + "_vs_" + e.bowler)) {
        obj[e.batsman + "_vs_" + e.bowler]++;
      } else {
        obj[e.batsman + "_vs_" + e.bowler] = 1;
      }
    }
  });

  let max = 0;
  let str = "";
  for (key in obj) {
    if (obj[key] > max) {
      max = obj[key];
      str = key;
    }
  }

  return `Highest Dismissal Pair in whole ipl seasons is ${str} : ${max} times`;
}

// Find the bowler with the best economy in super overs

function bestEconomyinSuperOver(DeliveryData) {
  let obj = {};

  let delivery = "delivery";

  DeliveryData.map((e) => {
    if (e.is_super_over == 1) {
      if (obj.hasOwnProperty(e.bowler)) {
        const runs =
          parseInt(e.batsman_runs) +
          parseInt(e.wide_runs) +
          parseInt(e.noball_runs);
        console.log(runs, e.is_super_over);
        obj[e.bowler].run += runs;
        if (e.noball_runs == 0 && e.wide_runs == 0) {
          obj[e.bowler].delivery += 1;
        }
      } else {
        obj[e.bowler] = {
          run:
            parseInt(e.batsman_runs) +
            parseInt(e.wide_runs) +
            parseInt(e.noball_runs),
          delivery: 1,
        };
      }
    }
  });

  let min = Number.MAX_VALUE;
  let bowler = "";
  for (let key in obj) {
    let overno = obj[key][delivery] / 6;
    let rem = obj[key][delivery] % 6;

    let over = overno + rem / 10;

    let economy = obj[key].run / over;
    if (economy < min) {
      min = economy;
      bowler = key;
    }
  }

  return `player with best economy in super over is ${bowler}`;
}
/* 
function filterDatabyseason(matchdata, deliverydata) {
  let winner = [];
  let obj = {};
  console.log("a");
  id = matchdata
    .filter(
      (e) => e.team1 == "Delhi Daredevils" && e.team2 == "Kings XI Punjab"
    )
    .map((e) => winner.push(["winner", e.winner]));
  const data = [
    ["Rahul", 23],
    ["Vikky", 27],
    ["Sanjay", 29],
    ["Jay", 19],
    ["Dinesh", 21],
    ["Sandeep", 45],
    ["Umesh", 32],
    ["Rohit", 28],
  ];

  //   const constructObject = winner => {
  //     return winner.reduce((acc, val) => {
  //        const [key, value] = val;
  //        acc[key] = value;
  //        return acc;
  //     }, {});
  //  }
  const a = winner.reduce((acc, val) => {
    const [key, value] = val;
    acc[key] = value;
    console.log(acc);
    return acc;
  }, {});

  //console.log(winner)
  // let a= constructObject(winner)
  return a;
}
 */
/* function kholiRunsbyseason(matchdata, deliverydata) {
  let id = matchdata.filter((e) => e.season == "2017").map((e) => e.id);
  console.log(id.indexOf("60"));
  let input1 = deliverydata.filter((e) => id.indexOf(e.match_id) > -1);
  console.log(input1[input1.length - 1]);

  let obj = {};
  let runs = 0;
  let boundary = 0;
  let c = input1
    .filter((e) => e.batsman == "S Dhawan")
    .map((e) => {
      runs += Number(e.batsman_runs);
    });

  let d = input1
    .filter(
      (e) =>
        e.batsman == "S Dhawan" && e.batsman_runs >= 4 && e.batsman_runs < 6
    )
    .map((e) => {
      boundary += 1;
    });

  //console.log(boundary)
} */

/* unction runsbyKholiallseason(matchData, deliveryData) {
  let seasonArray = matchData.map((e) => e.season);
  console.log(seasonArray);
  let seasons = [...new Set(seasonArray)].sort();
  console.log(seasons);
  let obj = {};
  for (let i = 0; i < seasons.length; i++) {
    let id = matchData.filter((e) => e.season == seasons[i]).map((e) => e.id);

    var x = deliveryData
      .filter((e) => id.includes(e.match_id) && e.batsman == "V Kohli")
      .map((e) => {
        obj[seasons[i]]
          ? (obj[seasons[i]] += +e.batsman_runs)
          : (obj[seasons[i]] = +e.batsman_runs);
      });
    //  console.log(x)
  }

  // console.log(obj)
} */

/* function top10rungettersin2013(matchData, deliveryData) {
  let idRange = matchData.filter((e) => e.season == "2013").map((e) => e.id);

  let data = deliveryData.filter((e) => idRange.indexOf(e.match_id) > -1);

  let obj = {};

  data.map((e) =>
    obj[e.batsman]
      ? (obj[e.batsman] += +e.batsman_runs)
      : (obj[e.batsman] = +e.batsman_runs)
  );

  const sortable = Object.entries(obj)
    .sort(([, a], [, b]) => b - a)
    .slice(0, 10)
    .reduce((r, [k, v]) => ({ ...r, [k]: v }), {});

  //console.log(sortable)
} */

//"player_dismissed": "",
//"dismissal_kind": "",

/* nction top10wicketTakersin2011(matchData, deliveryData) {
  let obj = {};
  let idRange = matchData.filter((e) => e.season == "2013").map((e) => e.id);

  let data = deliveryData
    .filter((e) => idRange.indexOf(e.match_id) > -1)
    .map((e) => {
      if (obj[e.bowler]) {
        if (e.player_dismissed.length != 0 && e.dismissal_kind != "run out") {
          obj[e.bowler]++;
        }
      } else {
        if (e.player_dismissed.length != 0 && e.dismissal_kind != "run out") {
          obj[e.bowler] = 1;
        }
      }
    });

  let ans = Object.entries(obj)
    .sort((a, b) => b[1] - a[1])
    .slice(0, 10)
    .reduce((r, [k, v]) => ({ ...r, [k]: v }), {});

  //console.log(ans)
} */
/* 
function boundariesHitbyKholiin2017(matchdata, deliverydata) {
  let obj = {};

  let idRange = matchdata.filter((e) => e.season == "2013").map((e) => e.id);

  let data = deliverydata
    .filter(
      (e) =>
        idRange.indexOf(e.match_id) > -1 &&
        e.batsman == "V Kohli" &&
        (e.batsman_runs == 4 || e.batsman_runs == 6)
    )
    .reduce((obj, e) => {
      obj[e.batsman_runs] ? obj[e.batsman_runs]++ : (obj[e.batsman_runs] = 1);
      return obj;
    }, {});

  console.log(data);
} */

/* 
 function playerWhoHitBoundries(deliveriesData, matchData)
{
    let match2017Data = matchData.filter(matchobj =>{
        return matchobj.season === "2017"
     }).map(obj => obj.id);
    // console.log(match2017Data);

     let hitBoundryCount = deliveriesData.filter((deliveryObj)=>{
        return (match2017Data.includes(deliveryObj.match_id) && deliveryObj.batsman === "V Kohli") && (deliveryObj.batsman_runs == 4 ||deliveryObj.batsman_runs == 6)
    })
    .reduce((tempObj,deliveryObj)=>{
        if(tempObj.hasOwnProperty(deliveryObj.batsman_runs))
        {
            tempObj[deliveryObj.batsman_runs] += 1;

        }
        else{
            tempObj[deliveryObj.batsman_runs] = 1;
        }
        return tempObj;
    },{})

    return hitBoundryCount;
          */

// top 10 wicket takers in all the seasons
/* "match_id": "510",
    "inning": "2",
    "batting_team": "Chennai Super Kings",
    "bowling_team": "Royal Challengers Bangalore",
    "over": "18",
    "ball": "2",
    "batsman": "MS Dhoni",
    "non_striker": "F du Plessis",
    "bowler": "SB Jakati",
    "is_super_over": "0",
    "wide_runs": "0",
    "bye_runs": "0",
    "legbye_runs": "0",
    "noball_runs": "0",
    "penalty_runs": "0",
    "batsman_runs": "6",
    "extra_runs": "0",
    "total_runs": "6",
    "player_dismissed": "",
    "dismissal_kind": "",
    "fielder": ""


 */
/* 
function top10wicketTakersinalltheSeasons(matchData, DeliveryData) {
  let seasonArr = matchData.map((e) => e.season);

  let arr = [...new Set(seasonArr)].sort();

  //console.log(arr)
  console.log(arr);

  let obj = {};
  let final_Obj = {};
  for (let i = 0; i < arr.length; i++) {
    obj[arr[i]] = {};

    let idRange = matchData.map((e) => {
      if (e.season == arr[i]) {
        return e.id;
      }
    });

    // console.log(idRange)

    DeliveryData.filter((e) => {
      //console.log(e)
      return (
        idRange.indexOf(e.match_id) > -1 &&
        e.player_dismissed.length != 0 &&
        e.dismissal_kind != "run out"
      );
    }).map((e) => {
      // console.log(obj[arr[i]][e.bowler])
      obj[arr[i]][e.bowler]
        ? obj[arr[i]][e.bowler]++
        : (obj[arr[i]][e.bowler] = 1);
    });

    let a = Object.entries(obj[arr[i]])
      .sort((a, b) => b[1] - a[1])
      .slice(0, 10)
      .reduce((r, [k, v]) => {
        r = {
          ...r, 
          [k]: v
        }

        return r;
      }, {});

    final_Obj[arr[i]] = a;
  }

  console.log(final_Obj);
} */

/* 
function bumbraWickettakeninALLseason(matchdata, deliverydata){

   let obj1 = {};
   let obj2 =  {}
   let winner={}
        
     
       let range=  matchdata.map((e)=>{ 
       
        if(e.season=='2014') { 
          
          obj1[e.winner]  ?  obj1[e.winner] ++ :  obj1[e.winner]=1 
                                         
          obj2[e.player_of_match] ? obj2[e.player_of_match] ++ : obj2[e.player_of_match]=1   

          if(e.toss_winner==e.winner)    { 
            
            winner[e.winner]? winner[e.winner]++ : winner[e.winner] =1  
        
        } 
                    
       }})  

      let range2= matchdata.filter((e)=>e.season==2013).map((e) => e.id)
      
      console.log(range2)
      let  data=deliverydata.filter((e)=>  range2.include(e.match_id) && e.bowler=='SL Malinga').map((e)=>{

          

      }
      
      )  
      //console.log(obj)


        let finalObj1=Object.entries(obj1).sort( (a,b)=> b[1]-a[1]).reduce((r,[k,v])=> ({...r,[k]:v}),{})
        let finalObj2=Object.entries(obj2).sort( (a,b)=> b[1]-a[1]).reduce((r,[k,v])=> ({...r,[k]:v}),{})

        console.log(winner)

} */