const http = require("http");
const fs = require("fs");
const path=require("path");
//const favicon = require('serve-favicon')exit


let server = http.createServer((req, res) => {
  
  switch (req.url) {
    
    case "/html":
        
      fs.readFile(

        path.join(__dirname, "../public/index.html"),
        "utf-8",
        (err, data) => {
          if (err) {
           
            res.writeHead(400, { "Content-Type": "text/html" });
            res.end(JSON.stringify({ data: "HTML file not found" }));
          } else if (data) {
         
            res.writeHead(200, { "Content-Type": "text/html" });
          
            res.end(data);
            
          }
        }
      );

      break;


    case "/script":
        
    fs.readFile(
      path.join(__dirname, "../public/script.js"),
        "utf-8",
        (err, data) => {
          if (err) {
            res.writeHead(400, { "Content-Type": "text/html" });
            res.end(JSON.stringify({ data: "HTML file not found" }));
          } else if (data) {
            res.writeHead(200, { "Content-Type": "text/html" });
            res.end(data);
          }
        }
      );
        
    
      break
    
    case "/css":
        
    fs.readFile(
      path.join(__dirname, "../public/style.css"),
        "utf-8",
        (err, data) => {
          if (err) {
            res.writeHead(400, { "Content-Type": "text/css" });
            res.end(JSON.stringify({ data: "HTML file not found" }));
          } else if (data) {
            res.writeHead(200, { "Content-Type": "text/css" });
            res.end(data);
          }
        }
      );
        
    
      break

    
      
    case "/matchplayedperYear.json":
     
    fs.readFile(
      path.join(__dirname, "../public/output/matchplayedperYear.json"),
      "utf-8",
      (err, data) => {
        if (err) {
          res.writeHead(400, { "Content-Type": "text/json" });
          res.end(JSON.stringify({ data: "HTML file not found" }));
        } else if (data) {
          res.writeHead(200, { "Content-Type": "text/json" });
          res.end(data);
        }
      }
    );
        
    
      break
    case "/tossWinData.json":
        
    fs.readFile(
      path.join(__dirname,"../public/output/tossWinData.json"),
        "utf-8",
        (err, data) => {
          if (err) {
            res.writeHead(400, { "Content-Type": "text/json" });
            res.end(JSON.stringify({ data: "HTML file not found" }));
          } else if (data) {
            res.writeHead(200, { "Content-Type": "text/json" });
            res.end(data);
          }
        }
      );
        
    
      break
    
    case "/teamWinPerYear.json":
        
    fs.readFile(
      path.join(__dirname,"../public/output/teamWinPerYear.json"),
        "utf-8",
        (err, data) => {
          if (err) {
            res.writeHead(400, { "Content-Type": "text/json" });
            res.end(JSON.stringify({ data: "HTML file not found" }));
          } else if (data) {
            res.writeHead(200, { "Content-Type": "text/json" });
            res.end(data);
          }
        }
      );
        
    
      break
    
    case "/extrarunsData2016.json":
        
    fs.readFile(
      path.join(__dirname,"../public/output/extrarunsData2016.json"),
        "utf-8",
        (err, data) => {
          if (err) {
            res.writeHead(400, { "Content-Type": "text/json" });
            res.end(JSON.stringify({ data: "HTML file not found" }));
          } else if (data) {
            res.writeHead(200, { "Content-Type": "text/json" });
            res.end(data);
          }
        }
      );
        
    
      break
    
 




    case "/top10economy2015.json":
        
    fs.readFile(
      path.join(__dirname,"../public/output/top10economy2015.json"),
        "utf-8",
        (err, data) => {
          if (err) {
            res.writeHead(400, { "Content-Type": "text/json" });
            res.end(JSON.stringify({ data: "HTML file not found" }));
          } else if (data) {
            res.writeHead(200, { "Content-Type": "text/json" });
            res.end(data);
          }
        }
      );
        
    
      break
    case "/strikeRateperSeason.json":
        
    fs.readFile(
      path.join(__dirname,"../public/output/strikeRateperSeason.json"),
        "utf-8",
        (err, data) => {
          if (err) {
            res.writeHead(400, { "Content-Type": "text/json" });
            res.end(JSON.stringify({ data: "HTML file not found" }));
          } else if (data) {
            res.writeHead(200, { "Content-Type": "text/json" });
            res.end(data);
          }
        }
      );
        
    
      break
    
      default:

        fs.readFile(

          path.join(__dirname, "../public/index.html"),
          "utf-8",
          (err, data) => {
            if (err) {
             
              res.writeHead(400, { "Content-Type": "text/html" });
              res.end(JSON.stringify({ data: "HTML file not found" }));
            } else if (data) {
           
              res.writeHead(200, { "Content-Type": "text/html" });
            
              res.end(data);
              
            }
          }
        );

  
      break  
    

  }
});
let port = process.env.PORT || 5400

server.listen(port, () => console.log("Server has Started"));
