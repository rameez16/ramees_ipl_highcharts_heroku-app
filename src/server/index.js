let deliveryData = require("../data/deliveries.json");
let matchData = require("../data/matches.json");

const fs = require("fs");
const functions = require("./ipl");
const path = require("path");

let matchPlayedYearly = functions.matchesPlayedYearly(matchData);
let teamWinData = functions.teamWinPerYear(matchData);
let extrRunsconceded = functions.extrRuns(matchData, deliveryData);
let top10economy = functions.topTenEconomicalBowlers(matchData,deliveryData);
let tossWin= functions.tossWinandMatchwin(matchData)
let mom= functions.highestMoMperSeason(matchData)
let strikeRateperSeason= functions.strikeRateperSeason(matchData,deliveryData)
let highestDismissalPair=functions.highestDismissalPair(deliveryData)
let bestEconomyinSuperOver=functions.bestEconomyinSuperOver(deliveryData)
/* let q1=functions.filterDatabyseason(matchData,deliveryData)
let q2=functions.kholiRunsbyseason(matchData,deliveryData)
let q3=functions.runsbyKholiallseason(matchData,deliveryData)
let q4=functions.top10rungettersin2013(matchData,deliveryData)
let q5=functions.top10wicketTakersin2011(matchData,deliveryData)
let q6=functions.boundariesHitbyKholiin2017(matchData,deliveryData)
let q7=functions.top10wicketTakersinalltheSeasons(matchData,deliveryData)
let q8=functions.bumbraWickettakeninALLseason(matchData,deliveryData) */
outputJason(matchPlayedYearly, "matchplayedperYear");
outputJason(teamWinData, "teamWinPerYear");
outputJason(extrRunsconceded, "extrarunsData2016");
outputJason(top10economy, "top10economy2015");
outputJason(tossWin, "tossWinData")
outputJason(mom,'momOfthematchPerSeason')
outputJason(strikeRateperSeason,'strikeRateperSeason')
outputJason(highestDismissalPair,'highestDismissalPair')
outputJason(bestEconomyinSuperOver,'superover economy')

function outputJason(inputData, filename) {
  
  let relPath = `../public/output/${filename}.json`;
  let absPath = path.join(__dirname, relPath);

  fs.writeFile(absPath, JSON.stringify(inputData, null, 2), (err) => {
    if (err) {
      console.log(err);
    } else {
      console.log("file written successfully");
    }
  });
}


console.log(__dirname)
