const csv = require("csvtojson");

const path = require("path");
const fs = require("fs");

const deliveriesFile = "./data/deliveries.csv";
const matchesFile = "./data/matches.csv";

function convertCSVtoJSON(csvfilepath, jsonFilename) {
  const absCSVfilepath = path.join(__dirname, csvfilepath);
  csv()
    .fromFile(absCSVfilepath)
    .then((jsonObj) => {
      outputJason(jsonObj, jsonFilename);
    });
}

function outputJason(inputData, filename) {
 
    let relPath = `data/${filename}.json`;
  let absPath = path.join(__dirname, relPath);
 
  fs.writeFile(absPath, JSON.stringify(inputData, null, 2), (err) => {
    if (err) {
      console.log(err);
    } else {
      console.log("file written successfully");
    }
  });
}

convertCSVtoJSON(matchesFile, "matches");
convertCSVtoJSON(deliveriesFile, "deliveries");

module.exports = outputJason;
