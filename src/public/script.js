matchperYear();
tossWinData();
teamWinperYear();
extrarunsData();
top10economicalPlayers();
strikeRateperSeason();

function matchperYear() {
  fetch("https://displayipldata.herokuapp.com/matchplayedperYear.json")
    .then((response) => response.json())
    .then((data) => {
      // console.log(arr)
      highChartBarGraph(
        data,
        "container",
        "MatchPlayed Vs Year",
        "Matches played in nos",
        "Matches played"
      );
    })
    .catch((err) => console.error(err));
}



function tossWinData() {

  fetch("https://displayipldata.herokuapp.com/tossWinData.json")
    .then((response) => response.json())
    .then((data) => {
      console.log('hi');
      highChartBarGraph(
        data,
        "container2",
        "TossWin & MatchWIn",
        "Matches won in nos",
        "Matches won"
      );
    })
    .catch((err) => console.error(err));
}

function teamWinperYear() {

  fetch("https://displayipldata.herokuapp.com/teamWinPerYear.json")
    .then((response) => response.json())
    .then((data) => {
      HighchartsStackedGraph(data);
    })
    .catch((err) => console.error(err));
}

function extrarunsData() {

  fetch("https://displayipldata.herokuapp.com/extrarunsData2016.json")
    .then((response) => response.json())
    .then((data) => {
      console.log(data);
      highChartBarGraph(
        data,
        "container4",
        "Extraruns Vs Teams",
        "extraruns",
        "runs"
      );
    })
    .catch((err) => {
      console.error(err);
    });
}

function top10economicalPlayers() {

  fetch("https://displayipldata.herokuapp.com/top10economy2015.json")
    .then((response) => response.json())
    .then((data) => {
      highChartBarGraph(
        data,
        "container5",
        "Top 10 economical players",
        "economy",
        "economy"
      );
    })
    .catch((err) => {
      console.error(err);
    });
}

function strikeRateperSeason() {
  fetch("https://displayipldata.herokuapp.com/strikeRateperSeason.json")
    .then((response) => response.json())
    .then((data) => {
      highChartLineGraph(data);
    });
}

function highChartBarGraph(data, divClass, title, yAxis, tooltip) {
  try {
    if (
      typeof data !== "object" ||
      typeof divClass !== "string" ||
      typeof tooltip !== "string" ||
      typeof title !== "string" ||
      typeof yAxis !== "string" ||
      typeof tooltip !== "string"
    ) {
      throw new Error("invalid data type received");
    }

    let arr = [];
    for (let key in data) {
      arr.push([key, data[key]]);
    }
    if (Object.prototype.toString.call(data) !== "[object Object]") {
      arr = data;
    }

    Highcharts.chart(divClass, {
      chart: {
        type: "column",
      },
      title: {
        text: title,
      },
      subtitle: {
        text: 'Source: <a href="http://en.wikipedia.org/wiki/List_of_cities_proper_by_population">ipl.com</a>',
      },
      xAxis: {
        type: "category",
        labels: {
          rotation: -45,
          style: {
            fontSize: "13px",
            fontFamily: "Verdana, sans-serif",
          },
        },
      },
      yAxis: {
        min: 0,
        title: {
          text: yAxis,
        },
      },
      legend: {
        enabled: false,
      },
      tooltip: {
        pointFormat: `${tooltip}: <b>{point.y:.1f} nos</b>`,
      },
      series: [
        {
          name: "Population",
          data: arr,
          dataLabels: {
            enabled: true,
            rotation: 0,
            color: "#FF0000",
            align: "right",
            format: "{point.y:.1f}", // one decimal
            y: 20, // 10 pixels down from the top
            style: {
              fontSize: "10px",
              fontFamily: "Verdana, sans-serif",
            },
          },
        },
      ],
    });
  } catch (err) {
    console.error(err);
  }
}

function highChartLineGraph(data) {
  if (Object.prototype.toString.call(data) !== "[object Object]") {
    throw new Error("Invalid data received");
  }

  const sliced = Object.keys(data)
    .slice(0, 7)
    .reduce((result, key) => {
      result[key] = data[key];

      return result;
    }, {});

  let newArray = [];

  for (let key in sliced) {
    //newArray.push([key,sliced[key]])
    obj = {};

    obj["name"] = key;
    let arrX = [];

    for (let key2 in sliced[key]) {
      arrX.push(sliced[key][key2].Strikerate);
    }
    obj["data"] = arrX;

    newArray.push(obj);
  }

  //console.log(newArray)

  Highcharts.chart("container6", {
    title: {
      text: "Strike Rate vs Year",
    },

    subtitle: {
      text: "Source: ipl.com",
    },

    yAxis: {
      title: {
        text: "strike rate",
      },
    },

    xAxis: {
      accessibility: {
        rangeDescription: "Range: 2010 to 2017",
      },
    },

    legend: {
      layout: "vertical",
      align: "right",
      verticalAlign: "middle",
    },

    plotOptions: {
      series: {
        label: {
          connectorAllowed: false,
        },
        pointStart: 2008,
      },
    },

    series: newArray,

    responsive: {
      rules: [
        {
          condition: {
            maxWidth: 500,
          },
          chartOptions: {
            legend: {
              layout: "horizontal",
              align: "center",
              verticalAlign: "bottom",
            },
          },
        },
      ],
    },
  });
}

function HighchartsStackedGraph(data) {
  if (Object.prototype.toString.call(data) !== "[object Object]") {
    throw new Error("Invalid data received");
  }

  let arr1 = [];
  arr1.push(data);
  let category = Object.keys(data);

  let year = [
    "2008",
    "2009",
    "2010",
    "2011",
    "2012",
    "2013",
    "2014",
    "2015",
    "2016",
    "2017",
  ];

  var win = new Array(category.length);
  let finalArr = [];

  for (let i = 0; i < category.length; i++) {
    win = [];
    for (let j = 0; j < year.length; j++) {
      arr1.map((e) => {
        if (e[category[i]][year[j]]) {
          //console.log(e)
          win.push(e[category[i]][year[j]]);
        } else {
          win.push(0);
        }
      });
    }
    console.log(win);
    let temp = win;

    //windata.push(win)
    let obj = {};
    obj["name"] = year[i];
    obj["data"] = temp;

    finalArr.push(obj);
  }

  // for(let i=0;i<category.length; i++){

  //          }

  // console.log("hello1", windata)

  Highcharts.chart("container3", {
    chart: {
      type: "column",
    },
    title: {
      text: "Matches Won per Team Per Year",
    },
    xAxis: {
      categories: category,
    },
    yAxis: {
      min: 0,
      title: {
        text: "Matches Won",
      },
      reversedStacks: false,
      stackLabels: {
        enabled: true,
        style: {
          fontWeight: "bold",
          color:
            // theme
            (Highcharts.defaultOptions.title.style &&
              Highcharts.defaultOptions.title.style.color) ||
            "gray",
        },
      },
    },
    legend: {
      align: "right",
      x: -30,
      verticalAlign: "top",
      y: 25,
      floating: true,
      backgroundColor:
        Highcharts.defaultOptions.legend.backgroundColor || "white",
      borderColor: "#CCC",
      borderWidth: 1,
      shadow: false,
    },
    tooltip: {
      headerFormat: "<b>{point.x}</b><br/>",
      pointFormat: "{series.name}: {point.y}<br/>Total: {point.stackTotal}",
    },
    plotOptions: {
      column: {
        stacking: "normal",
        dataLabels: {
          enabled: true,
        },
      },
    },
    series: finalArr,
  });
}
